# EpicAuth Loader

Loader for EpicAuth.

## Using
1. Download EpicAuth from here: https://gitlab.com/TheTank3753/EpicAuth-Loader/-/releases
2. Download a game that uses EOS from the Epic Games Store.
3. Replace the game executable with EpicAuth, and rename it
4. Launch the game, and set the path.
5. Launch it again.

## Building
1. Download the source.
2. Open it with Visual Studio 2019 (ReSharper is recommended).
3. Build it in "Build -> Build EpicAuth". For a more complete solution, right click the project and click "Publish".
